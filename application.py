from flask import Flask
 
app = Flask(__name__)
 
@app.route('/')
def hello_world():
    return 'This is the second commit from application'
 
if __name__ == '__main__': 
    app.run(debug=True)
