FROM python

#need to install docker cli on top of dockerimage

RUN curl -fsSL https://get.docker.com -o get-docker.sh
RUN sh get-docker.sh --no-install-engine
